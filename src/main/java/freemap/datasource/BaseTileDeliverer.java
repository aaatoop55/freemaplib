package freemap.datasource;

import java.util.HashMap;
import java.util.ArrayList;


import freemap.data.Point;
import freemap.data.Projection;


import java.util.Map;
import java.util.Set;


//The BaseTileDeliverer/CachedTileDeliverer system is intended to replace the plain TileDeliverer
//to allow for loading from a database. However TileDeliverer still in use.

// 25/02/18 - 26/02/18 added empty tile tracker
// when downloading tiles, some might end up failing (due to exception)
// implemented an isEmptyData() on TiledData, empty tiles are checked and
// added to an ArrayList and can then be updated later.
// as part of this doUpdate() had to be provided with a flag which can force
// reload; in an empty tiles situation we want to force all to be reloaded.
// This flag also had to be added to CachedTileDeliverer (doUpdate() overridden)
//
// 28/02/18 changed wording above from empty to failed.
// An empty tile will NOT be an error condition, it will be due to there being
// no data! Failed downloads will always raise an exception. We now explicitly
// set entries in 'data' to null if an exception occurs and use this to find
// our failed tiles. 

public abstract class BaseTileDeliverer {
    
    protected int tileWidth, tileHeight;
    protected Point curPos, prevPos;
    
    protected HashMap<String,TiledData> data;
	protected HashMap<String, Tile> partiallyUpdatedData;
    
    protected Projection proj;
    protected String name;
    
    // in case our projection is WGS84/4326 but we want to use microdegrees for tiles so we
    // can deal in whole numbers
    protected double tileMultiplier; 

    
    public BaseTileDeliverer(String name,int tileWidth,int tileHeight,Projection proj)
    {
        this(name,tileWidth,tileHeight,proj,1.0);
    }
    
    public BaseTileDeliverer(String name,int tileWidth,int tileHeight,Projection proj,double multiplier)
    {
        data=new HashMap<String,TiledData>();
    
        this.tileWidth=tileWidth;
        this.tileHeight=tileHeight;
        this.proj=proj;
        this.name=name;
        this.tileMultiplier = multiplier;
    }
    public TiledData update(Point lonLat) throws Exception
    {
        Point newPos = proj.project(lonLat);
        // IMPORTANT curPos is projected but has no multiplier applied
        TiledData curData=null;
        if(curPos==null || isNewObject(curPos,newPos))
        {   
            curPos = newPos;
            Point origin = getOrigin(newPos);
            
            Tile t = doUpdate(origin);
            curData = t.data;
        }
        else
        {
            curPos = newPos;    
        }
        
        // this is just arbitrary data
        return curData;
    }
    
    
    
    public TiledData updateSurroundingTiles(Point lonLat) throws Exception
    {
        HashMap<String,Tile> updatedData = doUpdateSurroundingTiles(lonLat);
            
        Point curOrigin = getOrigin(curPos);
        return (updatedData.get(""+(int)curOrigin.x+"." + (int)curOrigin.y)!=null)?
                    updatedData.get(""+(int)curOrigin.x+"." + (int)curOrigin.y).data : null;
    }
    
    
    // 24/02/13 now returns an array of Tiles.
    // This is to allow external manipulation of data e.g. applying a DEM then saving the data, where the
    // DEM-applied data is cached (such as in hikar)
    public HashMap<String,Tile> doUpdateSurroundingTiles(Point lonLat) throws Exception
    {
        HashMap<String,Tile> updatedData = new HashMap<String,Tile>();
        Point newPos = proj.project(lonLat);
		partiallyUpdatedData = null; 
        
        Point curOrigin = null;
        
        if(curPos==null || isNewObject(curPos,newPos))
        {   
            curPos = newPos;
            Point origin = getOrigin(newPos);
            for(int row=-1; row<=1; row++)
            {
                for(int col=-1; col<=1; col++)
                {
                    curOrigin = new Point(origin.x+col*tileWidth,
                                    origin.y+row*tileHeight);
                    
                    String key = ""+(int)curOrigin.x+"." + (int)curOrigin.y;
					try {
						Tile t  = doUpdate(curOrigin);
						updatedData.put(key, t);
					} catch(Exception e) {
						// if there was an exception with a given tile we want to quit but return what we did manage to get
						partiallyUpdatedData = updatedData;
						throw e;
					}
                } 
            }
            
        }
        else
        {
            
            curPos = newPos;    
        }
        return updatedData;
    }

	public HashMap<String,Tile> getPartiallyUpdatedData() 
	{
		return partiallyUpdatedData;
	}

	public HashMap<String,Tile> doUpdateFailedTiles(ArrayList<Point> failedOrigins) throws Exception
	{
		HashMap<String,Tile> updatedData = new HashMap<String,Tile>();
		for (int i=0; i<failedOrigins.size(); i++)
		{
			Point curOrigin = failedOrigins.get(i);
			Tile t = doUpdate(curOrigin, true);
			if(t!=null && t.data!=null)
			{
				String key = ""+(int)curOrigin.x+"." + (int)curOrigin.y;
				updatedData.put(key, t);
			}
		} 
		return updatedData;
	}

    
    // sets position without downloading data
    // e.g. after a forceDownload()
    public void setPosition(Point lonLat)
    {
        curPos = proj.project(lonLat);
    }
    
    
	protected Tile doUpdate(Point origin) throws Exception
	{
		return doUpdate(origin, false);	
	}

	// 260218 added forceReload e.g. when loading failed tiles
    protected Tile doUpdate(Point origin, boolean forceReload)throws Exception
    {
        TiledData curData=null;
        String key = "" + ((int)origin.x)+"."+((int)origin.y);
        
        if(!data.containsKey(key) || data.get(key)==null || forceReload)
        {
            curData = getDataFromSource(origin); 
            data.put(key,curData);
        }
        else
        {
            curData=data.get(key);
        }
        
        return new Tile(origin, curData, false);
    }

    public boolean needNewData(Point lonLat)
    {
        return curPos==null || isNewObject(curPos,proj.project(lonLat));
    }
    
    protected boolean isNewObject(Point oldPos, Point newPos)
    {
        Point bottomLeftOld = getOrigin(oldPos), bottomLeftNew = getOrigin(newPos);
        return !(bottomLeftOld.equals(bottomLeftNew));
    }
    
    protected Point getOrigin(Point p)
    {
        return (p==null) ? null:new Point(Math.floor((p.x*tileMultiplier)/tileWidth)*tileWidth,
                Math.floor((p.y*tileMultiplier)/tileHeight)*tileHeight);
    }
    
    public TiledData getData()
    {
        if(curPos!=null)
        {
            Point bottomLeft = getOrigin(curPos);
            String key = "" + ((int)bottomLeft.x)+"."+((int)bottomLeft.y);
            return data.get(key);
        }
        return null;
    }
    
    public Set<Map.Entry<String,TiledData> > getAllTiles()
    {
        return data.entrySet();
    }
    
    protected TiledData dataWrap(Point origin,Object rawData)
    {
        return (TiledData)rawData;
    }
    
    // NEW
    // tested
    public TiledData getData(Point p)
    {
        Point bottomLeft = getOrigin(p);
        String key = "" + ((int)bottomLeft.x)+"."+((int)bottomLeft.y);
        return data.get(key);
    }
        
    public Projection getProjection()
    {
        return proj;
    }

    // 260218 added
	// finds failed surrounding tiles. Does this by finding all surrounding
	// tiles with nothing in 'data'
    public ArrayList<Point> getFailedSurroundingTiles(Point lonLat) {
		ArrayList<Point> failedOrigins = new ArrayList<Point>();
        Point p = proj.project(lonLat);
        Point origin = getOrigin(p), curOrigin;
		String key = "";
        for(int row=-1; row<=1; row++) {
            for(int col=-1; col<=1; col++) {
                curOrigin = new Point(origin.x+col*tileWidth,
                                    origin.y+row*tileHeight);
                    
        		key = "" + ((int)curOrigin.x)+"."+((int)curOrigin.y);
				if(!data.containsKey(key))
				{
					failedOrigins.add(curOrigin);
				}
            }
        }
		return failedOrigins; 
    }


    public abstract TiledData getDataFromSource(Point origin) throws Exception;
}
