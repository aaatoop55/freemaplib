package freemap.datasource;

import java.util.HashMap;


import freemap.data.Point;
import freemap.data.Projection;

import java.io.File;

// The BaseTileDeliverer/CachedTileDeliverer system is intended to replace the plain TileDeliverer
// to allow for loading from a database. However TileDeliverer still in use.

public class CachedTileDeliverer extends BaseTileDeliverer {
    
    
    DataSource dataSource;
    
    DataInterpreter interpreter;
    
    String cachedir;
    
    private boolean cacheData, forceReload, reprojectCachedData;
    

    public CachedTileDeliverer (String name,DataSource ds, DataInterpreter interpreter,int tileWidth,int tileHeight,
                            Projection proj,String cachedir)
    {
        this(name,ds,interpreter,tileWidth,tileHeight,proj,cachedir,1.0);
    }
    
    public CachedTileDeliverer(String name,DataSource ds, DataInterpreter interpreter,int tileWidth,int tileHeight,
                            Projection proj,String cachedir,double multiplier)
    {
        super (name, tileWidth, tileHeight, proj, multiplier);
        dataSource=ds;
        this.interpreter=interpreter;    
        this.cachedir=cachedir;
        
        cacheData = true;
        forceReload = false;
        reprojectCachedData = true;
    }
    
    public void setCache(boolean cache)
    {
        cacheData = cache;
    }
    
    public void setForceReload(boolean fr)
    {
        forceReload = fr;
    }
    
    public void setReprojectCachedData(boolean rp)
    {
        reprojectCachedData = rp;
    }
        
    public TiledData updateSurroundingTiles(Point lonLat) throws Exception
    {
        
            
            
            HashMap<String,Tile> updatedData = doUpdateSurroundingTiles(lonLat);
            
            Point curOrigin = getOrigin(curPos);
            
          
            
            return (updatedData.get(""+(int)curOrigin.x+"." + (int)curOrigin.y)!=null)?
                    updatedData.get(""+(int)curOrigin.x+"." + (int)curOrigin.y).data : null;
            
    }
    
    
    
    protected String getCacheFile()
    {
        return getCacheFile(curPos);
    }
    
    
    protected String getCacheFile(Point p)
    {
        Point origin=getOrigin(p);
        String key="" + ((int)origin.x)+"."+((int)origin.y);
        return cachedir+"/"+name+"."+key;
    }
    
    
	// 260218 added forceReload e.g. when loading empty tiles
    protected Tile doUpdate(Point origin, boolean forceReload) throws Exception
    {
        TiledData curData=null;
        String key = "" + ((int)origin.x)+"."+((int)origin.y);
        
        if(!data.containsKey(key) || forceReload)
        {
           
            String cachefile=cachedir+"/"+name+"."+key;
            
            if(cachedir!=null && isCache(cachefile) && !forceReload)
            {
                curData = loadFromCache(cachefile,origin);
            }
            else
            {
                
				try
				{
                	curData = getDataFromSource(origin, cacheData ? cachefile: null);
				} 
				catch(Exception e)
				{
					// 020318 in case cache file is partly written when 
					// exception occurs
					deleteCacheFile(origin);
					throw e;
				}
                
                // It is assumed the data is in standard 4326 and we 
                // reproject on the client side. This is because it's a bit of
                // a pain to reproject into arbitrary projections server side
                // due to lack of a PHP Proj.4 library, whereas there is one for Java.
				curData.reproject(proj);
                //System.out.println(curData);
            }
            
            
			data.put(key,curData);
        }
        else
        {
            curData=data.get(key);
        }
        
        return new Tile(origin,curData,isCache(cachedir+"/"+name+"."+key));
    }

    // 180218 added
    public boolean deleteCacheFile(Point origin) {
        String key = "" + ((int)origin.x)+"."+((int)origin.y);
        String cachefile=cachedir+"/"+name+"."+key;
        File f= new File(cachefile);
		return f.exists() ? f.delete(): false;
    }    
    
    // 180218 added
    public int doDeleteSurroundingCachedTiles(Point lonLat) {
        int nSuccessful = 0;
        Point p = proj.project(lonLat);
        Point origin = getOrigin(p), curOrigin;
        for(int row=-1; row<=1; row++) {
            for(int col=-1; col<=1; col++) {
                curOrigin = new Point(origin.x+col*tileWidth,
                                    origin.y+row*tileHeight);
                    
                nSuccessful += deleteCacheFile(curOrigin) ? 1:0;
            }
        }
        return nSuccessful;
    }

    // 220913 deleted all the cache() and cacheByKey() methods, I don't think we need
    // them anymore, they are in TileDeliverer in case they turn out to be needed...
    
    // we need these two to do later caching e.g. after applying a DEM
    
    public void cache(TiledData data,String cachefile) throws Exception
    {
      
        if(cachedir!=null && data!=null)
        {   
            data.save(cachefile);   
        }
    }
    
    public void cacheByKey(TiledData data, String key) throws Exception
    {
        cache(data,cachedir+"/"+name+"."+key);
    }
    
    protected TiledData loadFromCache(String cachefile,Point origin) throws Exception
    {
        TiledData curData = null;
        
        FileDataSource ds = new FileDataSource(cachefile);
        curData = dataWrap(origin,ds.getData(interpreter));
        
        
        // 220913 We now need to reproject cached data as we are dumping the data straight to cache
        // on loading from the web, unless the data was cached later (e.g. applying a DEM)
        if(reprojectCachedData)
            curData.reproject(proj);
        return curData;
    }
    
    public boolean isCache()
    {
        return curPos!=null && isCache(getCacheFile());
    }
    
    public boolean isCache(Point lonLat)
    {
        String cacheFile=getCacheFile(proj.project(lonLat));
        return new File(cacheFile).exists();
    }
    
    public boolean isCache(String cachefile)
    {
        Point origin = getOrigin(curPos);
        return (origin==null) ? false: new File(cachefile).exists();
    }

    public void forceDownload(Point bottomLeft, Point topRight) throws Exception
    {
        Point blProjected = proj.project(bottomLeft);
        blProjected.x = Math.floor(blProjected.x/tileWidth) * tileWidth * tileMultiplier;
        blProjected.y = Math.floor(blProjected.y/tileHeight) * tileHeight * tileMultiplier;
        Point trProjected = proj.project(topRight);
        trProjected.x = Math.floor(trProjected.x/tileWidth) * tileWidth * tileMultiplier;
        trProjected.y = Math.floor(trProjected.y/tileHeight) * tileHeight * tileMultiplier;
        Point curPoint = new Point(blProjected.x,blProjected.y);
        while((int)curPoint.x <= (int)trProjected.x)
        {
            
            curPoint.y = blProjected.y;
            while((int)curPoint.y <= (int)trProjected.y)
            {
                doUpdate(curPoint);
                curPoint.y += tileHeight;
            }
            curPoint.x += tileWidth;
        }
    }
    
    public TiledData getDataFromSource (Point origin) throws Exception
    {
        return getDataFromSource (origin, null);
    }
    
    public TiledData getDataFromSource (Point origin, String cacheFile) throws Exception
    {
       TiledData td = dataWrap(origin,dataSource.getData(origin,interpreter,cacheFile));
		return td;
    }
}
