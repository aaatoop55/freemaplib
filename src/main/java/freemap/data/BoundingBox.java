package freemap.data;

public class BoundingBox {
    public Point bottomLeft, topRight;

    public BoundingBox(double w, double s, double e, double n) {
        bottomLeft = new Point(w,s);
        topRight = new Point(e,n);
    }

    public boolean intersects(BoundingBox other) {
        return intersects(other, 0.000001);
    }

    public boolean intersects(BoundingBox other, double extension) {
        BoundingBox left = bottomLeft.x<other.bottomLeft.x ? this: other,
            right = left==this ? other: this,
            bottom = bottomLeft.y<other.bottomLeft.y ? this:other,
            top = bottom==this ? other: this;

        return left.topRight.x+extension > right.bottomLeft.x-extension &&
                bottom.topRight.y+extension > top.bottomLeft.y-extension;
    }
}
