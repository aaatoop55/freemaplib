package freemap.jdem;

import freemap.datasource.FileFormatter;
import freemap.data.Point;

public class SRTMMicrodegFileFormatter extends FileFormatter {

    int width, height;
    
    public SRTMMicrodegFileFormatter()
    {
        this(100000, 50000);
    }
   
 
    public SRTMMicrodegFileFormatter(int width, int height)
    {
        this.width=width;
        this.height=height;
    }
    
    public String format (Point p)
    {
        return "?bbox=" + ((int)p.x) + "," + ((int)p.y) + "," +
                        ((int)p.x + width) + "," + ((int)p.y + height);        
    }
}
